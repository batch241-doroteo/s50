import {useState, useEffect} from 'react'

// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';



export default function Courses() {

	// State that will be use to store the courses retrieve from the database.
	const [course, setCourses] = useState([])

	// Retrieve the courses from the database upon initial render of the "Courses component"

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/activeCourse`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course =>{
				return (
					<CourseCard key={course.id} courseProp = {course} />
					)
			}))
		})
	}, [])


	// Checks to see if the mock data was captured.
	// console.log(coursesData) // to check all the information of data.
	// console.log(coursesData[0]) // to check the specific index

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp = {course} />
	// 		)
	// })
	
	return (
		<>

		{course}

		{/*Props drilling - we can pass information from one component to another using props*/}
		{/* {} - used in props to signify that we are providing information*/}
		{/*<CourseCard courseProp = {coursesData[0]} />*/}

		</>
		)
}