// import {Link } from 'react-router-dom';
//  // to link the certain tab on the Navbar

// import {Button, Row, Col} from 'react-bootstrap';

// export default function errorPage() {

// // check if the path is not declared

// 	return (
// 		<Row>
// 		<Col className="p-5">
// 		<h1>Error 404 - Page not found</h1>
// 		<Link to="/">
//         <Button variant="primary">Go to home Page</Button>
//       </Link>
// 		</Col>
// 		</Row>
// 		)


// }

import Banner from '../components/Banner';

export default function Error() {
	const data = {
		title: "Error 404 - Page Not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}
	return (
		<Banner data={data}/>
	)
}
