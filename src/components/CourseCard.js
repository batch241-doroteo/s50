import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}) {
  
  const {name, description, price, _id} = courseProp

  // // Use the state hook for this component to be able to store its state
  // // States are used to keep track of information related to individual components
  // /*
	// SYNTAX:
	// const [getter, setter] = useState(initialGetterValue)
  // */
  // const [count, setCount] = useState(0); // Don't forget to declare import
  // const [seat, setSeat] = useState(30);
  // const [isOpen, setIsOpen] = useState(true)

  // console.log(useState)

  // function enroll() {
  // 	if (count + 1 <= 30 && seat - 1 >= 0) {
  //     setCount(count + 1);
  //     setSeat(seat - 1);
  //   }

  //   // if (count === 30) {
  //   // 	alert ('no more seats available')
  //   // }
  // }

  //     // USE EFFECT - will allow us to execute a function if the value of seat state changes.
  // useEffect(() => {
  //     if(seat === 0) {
  //       setIsOpen(false)
  //       document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true)
  //     }
  //     // will run anytime one of the values in the array of the dependencies changes
  //   }, [seat])

  return (

    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text> PHP {price}</Card.Text>
       {/* <Card.Text> Enrollees: {count}</Card.Text>
        <Card.Text> Seats available: {seat}</Card.Text>*/}

        {/*<Button variant="primary">Enroll</Button>*/}
        {/*<Button id={`btn-enroll-${id}`} className="bg-primary" onClick={enroll}>Enroll</Button>*/}
        <Button className='bg-primary' as={Link} to={`/courses/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
  );
}
